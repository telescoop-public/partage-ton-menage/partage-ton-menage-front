export interface Task {
  duration: number
  everyNWeek: number
  name: string
}
